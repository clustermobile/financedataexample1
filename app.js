/* eslint-disable linebreak-style */
/* eslint-disable no-console */

const libObj = require('./objectLib')
// Get data for Top 10 gainers/most active/losers/inFocus companies on iextrading.com
const gainersUrl = 'https://api.iextrading.com/1.0/stock/market/list/gainers'
// const mostactiveUrl = 'https://api.iextrading.com/1.0/stock/market/list/mostactive'
// const losersUrl = 'https://api.iextrading.com/1.0/stock/market/list/losers'
// const inFocusUrl = 'https://api.iextrading.com/1.0/stock/market/list/inFocus'

libObj.fetchData(gainersUrl)
.then((result) => {
    // Group companies by Primary exchange
    // Groups companies by sector
    if (result.length > 0){
        return Promise.all([libObj.getPrimaryExchange(result), libObj.getSector(result)])
    }    
    console.log(`The API call returned nothing!`)
    return result
})
.then(result => {
    if (result.length > 0){
        console.log(`This is Primary Exchange data: ${JSON.stringify(result[0], null, 2)}
        This is Sectors data: ${JSON.stringify(result[1], null, 2)}`)
    }
   else {
        console.log(`Oh NO...There is No data available!`)
    }
})