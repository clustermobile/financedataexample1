/* eslint-disable no-param-reassign */

const axios = require('axios')

module.exports = {
    getPrimaryExchange: function getPrimaryExchange(companiesArrayObj){
        return Promise.resolve(
            // Create an Array of object with "Primary Index" and "Company Name"
            companiesArrayObj.map((company) => {
                const primaryExchangeObj = {}
                const companyNameObj = {}
                let mapObj = {}
                primaryExchangeObj.primaryExchange = company.primaryExchange
                companyNameObj.companyName = company.companyName
                mapObj = { ...primaryExchangeObj, ...companyNameObj }
                return mapObj
            })
            // Create an Array of companies per "Primary index"
            .reduce((totalPrimaryExchangeObj, item) => {
                if (!totalPrimaryExchangeObj[item.primaryExchange]) {
                    totalPrimaryExchangeObj[item.primaryExchange] = [item.companyName]
                    return totalPrimaryExchangeObj
                }
                totalPrimaryExchangeObj[item.primaryExchange] = [...totalPrimaryExchangeObj[item.primaryExchange] , item.companyName]
                return totalPrimaryExchangeObj
            },{})
        )
    },
    getSector: function getSector(companiesArrayObj){
        return Promise.resolve(
        // Create an Array of objects with "Sector" and "Company Name"
        companiesArrayObj.map((company) => {
            const sectorObj = {}
            const companyNameObj = {}
            let mapObj = {}
            sectorObj.sector = company.sector
            companyNameObj.companyName = company.companyName
            mapObj = { ...sectorObj, ...companyNameObj }
            return mapObj
        })
        // Create an Array of companies per "Sector"
        .reduce((sectorObj, item) => {
            if (!sectorObj[item.sector]) {
                sectorObj[item.sector] = [item.companyName]
                return sectorObj
            }
            sectorObj[item.sector] = [...sectorObj[item.sector] , item.companyName]
            return sectorObj
        },{})
        )
    },
    fetchData: function fetchData(url){
        return Promise.resolve(
            axios
            .get(url)
            .then(response => {
                if (response.data !== []){
                    return response.data
                }
                console.log(`The API call returned nothing! Oh NO...There is No data available!`)
                return response.data
            })
            .catch(error => {
                console.error(`Oh no!!! An Error occured: 
                ${error}`)
            })
        )
    }
}