const axios = require('axios')
const libObj = require('./objectLib')

const mockStockData10 = [{"symbol":"GE", "companyName":"General Electric Company", "primaryExchange":"New York Stock Exchange", "sector":"Industrials", "calculationPrice":"close", "open":9.95, "openTime":1548945001769, "close":10.16, "closeTime":1548968419534, "high":10.77, "low":9.76, "latestPrice":10.16, "latestSource":"Close", "latestTime":"January 31, 2019", "latestUpdate":1548968419534, "latestVolume":345589242, "iexRealtimePrice":9.97, "iexRealtimeSize":400, "iexLastUpdated":1548971522099, "delayedPrice":10.16, "delayedPriceTime":1548968419534, "extendedPrice":10.03, "extendedChange":-0.13, "extendedChangePercent":-0.0128, "extendedPriceTime":1548971999679, "previousClose":9.1, "change":1.06, "changePercent":0.11648, "iexMarketPercent":0.01952, "iexVolume":6745902, "avgTotalVolume":120475699, "iexBidPrice":0, "iexBidSize":0, "iexAskPrice":0, "iexAskSize":0, "marketCap":88372848400, "peRatio":13.37, "week52High":16.275, "week52Low":6.66, "ytdChange":0.2469147826086955}, {"symbol":"AMD", "companyName":"Advanced Micro Devices Inc.", "primaryExchange":"Nasdaq Global Select", "sector":"Technology", "calculationPrice":"close", "open":23.04, "openTime":1548945000887, "close":24.41, "closeTime":1548968400286, "high":25.14, "low":22.83, "latestPrice":24.41, "latestSource":"Close", "latestTime":"January 31, 2019", "latestUpdate":1548968400286, "latestVolume":181936751, "iexRealtimePrice":24.43, "iexRealtimeSize":200, "iexLastUpdated":1548968396021, "delayedPrice":24.41, "delayedPriceTime":1548968400286, "extendedPrice":24.48, "extendedChange":0.07, "extendedChangePercent":0.00287, "extendedPriceTime":1548971997498, "previousClose":23.09, "change":1.32, "changePercent":0.05717, "iexMarketPercent":0.00751, "iexVolume":1366345, "avgTotalVolume":107153177, "iexBidPrice":0, "iexBidSize":0, "iexAskPrice":0, "iexAskSize":0, "marketCap":24395530143, "peRatio":69.74, "week52High":34.14, "week52Low":9.04, "ytdChange":0.28340473181094006}, {"symbol":"BAC", "companyName":"Bank of America Corporation", "primaryExchange":"New York Stock Exchange", "sector":"Financial Services", "calculationPrice":"close", "open":28.72, "openTime":1548945015451, "close":28.47, "closeTime":1548968801420, "high":28.84, "low":27.98, "latestPrice":28.47, "latestSource":"Close", "latestTime":"January 31, 2019", "latestUpdate":1548968801420, "latestVolume":100049797, "iexRealtimePrice":28.49, "iexRealtimeSize":5000, "iexLastUpdated":1548968396406, "delayedPrice":28.47, "delayedPriceTime":1548968801420, "extendedPrice":28.38, "extendedChange":-0.09, "extendedChangePercent":-0.00316, "extendedPriceTime":1548971994609, "previousClose":29.07, "change":-0.6, "changePercent":-0.02064, "iexMarketPercent":0.03442, "iexVolume":3443714, "avgTotalVolume":84921015, "iexBidPrice":0, "iexBidSize":0, "iexAskPrice":0, "iexAskSize":0, "marketCap":275284582954, "peRatio":10.91, "week52High":33.05, "week52Low":22.66, "ytdChange":0.14402346153846152}, {"symbol":"SIRI", "companyName":"Sirius XM Holdings Inc.", "primaryExchange":"Nasdaq Global Select", "sector":"Consumer Cyclical", "calculationPrice":"close", "open":5.92, "openTime":1548945000741, "close":5.83, "closeTime":1548968400758, "high":6.14, "low":5.82, "latestPrice":5.83, "latestSource":"Close", "latestTime":"January 31, 2019", "latestUpdate":1548968400758, "latestVolume":82606514, "iexRealtimePrice":5.84, "iexRealtimeSize":12, "iexLastUpdated":1548968394196, "delayedPrice":5.83, "delayedPriceTime":1548968400758, "extendedPrice":5.88, "extendedChange":0.05, "extendedChangePercent":0.00858, "extendedPriceTime":1548971996665, "previousClose":5.92, "change":-0.09, "changePercent":-0.0152, "iexMarketPercent":0.03669, "iexVolume":3030833, "avgTotalVolume":25135905, "iexBidPrice":0, "iexBidSize":0, "iexAskPrice":0, "iexAskSize":0, "marketCap":25517051742, "peRatio":24.29, "week52High":7.7, "week52Low":5.48, "ytdChange":-0.011810169491525496}, {"symbol":"FB", "companyName":"Facebook Inc.", "primaryExchange":"Nasdaq Global Select", "sector":"Technology", "calculationPrice":"close", "open":165.59, "openTime":1548945000264, "close":166.69, "closeTime":1548968400217, "high":171.68, "low":165, "latestPrice":166.69, "latestSource":"Close", "latestTime":"January 31, 2019", "latestUpdate":1548968400217, "latestVolume":76969347, "iexRealtimePrice":166.69, "iexRealtimeSize":12, "iexLastUpdated":1548969098806, "delayedPrice":166.63, "delayedPriceTime":1548968400225, "extendedPrice":166.9, "extendedChange":0.21, "extendedChangePercent":0.00126, "extendedPriceTime":1548971960495, "previousClose":150.42, "change":16.27, "changePercent":0.10816, "iexMarketPercent":0.03507, "iexVolume":2699315, "avgTotalVolume":26099155, "iexBidPrice":0, "iexBidSize":0, "iexAskPrice":0, "iexAskSize":0, "marketCap":479031657044, "peRatio":22.56, "week52High":218.62, "week52Low":123.02, "ytdChange":0.21679797169811305}, {"symbol":"NOK", "companyName":"Nokia Corporation Sponsored American Depositary Shares", "primaryExchange":"New York Stock Exchange", "sector":"Technology", "calculationPrice":"close", "open":6.39, "openTime":1548945180086, "close":6.35, "closeTime":1548968628948, "high":6.4, "low":6.21, "latestPrice":6.35, "latestSource":"Close", "latestTime":"January 31, 2019", "latestUpdate":1548968628948, "latestVolume":70136207, "iexRealtimePrice":6.35, "iexRealtimeSize":2300, "iexLastUpdated":1548968397613, "delayedPrice":6.35, "delayedPriceTime":1548968628948, "extendedPrice":6.4, "extendedChange":0.05, "extendedChangePercent":0.00787, "extendedPriceTime":1548971862365, "previousClose":6.57, "change":-0.22, "changePercent":-0.03349, "iexMarketPercent":0.02088, "iexVolume":1464444, "avgTotalVolume":25626251, "iexBidPrice":0, "iexBidSize":0, "iexAskPrice":0, "iexAskSize":0, "marketCap":35509397612, "peRatio":22.68, "week52High":6.65, "week52Low":4.75, "ytdChange":0.11110930313588852}, {"symbol":"P", "companyName":"Pandora Media Inc.", "primaryExchange":"New York Stock Exchange", "sector":"Consumer Cyclical", "calculationPrice":"close", "open":8.5, "openTime":1548945000726, "close":8.38, "closeTime":1548968429884, "high":8.83, "low":7.765, "latestPrice":8.38, "latestSource":"Close", "latestTime":"January 31, 2019", "latestUpdate":1548968429884, "latestVolume":69926773, "iexRealtimePrice":8.395, "iexRealtimeSize":300, "iexLastUpdated":1548968395528, "delayedPrice":8.38, "delayedPriceTime":1548968429884, "extendedPrice":8.45, "extendedChange":0.07, "extendedChangePercent":0.00835, "extendedPriceTime":1548971932722, "previousClose":8.49, "change":-0.11, "changePercent":-0.01296, "iexMarketPercent":0.00874, "iexVolume":611160, "avgTotalVolume":8538800, "iexBidPrice":0, "iexBidSize":0, "iexAskPrice":0, "iexAskSize":0, "marketCap":2260716101, "peRatio":null, "week52High":10.07, "week52Low":4.17, "ytdChange":0.0025902392344498565}, {"symbol":"CHK", "companyName":"Chesapeake Energy Corporation", "primaryExchange":"New York Stock Exchange", "sector":"Energy", "calculationPrice":"close", "open":2.91, "openTime":1548945001381, "close":2.85, "closeTime":1548968554405, "high":2.96, "low":2.77, "latestPrice":2.85, "latestSource":"Close", "latestTime":"January 31, 2019", "latestUpdate":1548968554405, "latestVolume":67898810, "iexRealtimePrice":2.835, "iexRealtimeSize":1040, "iexLastUpdated":1548968398413, "delayedPrice":2.85, "delayedPriceTime":1548968554405, "extendedPrice":2.83, "extendedChange":-0.02, "extendedChangePercent":-0.00702, "extendedPriceTime":1548971934589, "previousClose":2.9, "change":-0.05, "changePercent":-0.01724, "iexMarketPercent":0.0126, "iexVolume":855525, "avgTotalVolume":47117321, "iexBidPrice":0, "iexBidSize":0, "iexAskPrice":0, "iexAskSize":0, "marketCap":2604089209, "peRatio":2.91, "week52High":5.6, "week52Low":1.71, "ytdChange":0.30696091324200914}, {"symbol":"MSFT", "companyName":"Microsoft Corporation", "primaryExchange":"Nasdaq Global Select", "sector":"Technology", "calculationPrice":"close", "open":103.8, "openTime":1548945000922, "close":104.43, "closeTime":1548968400284, "high":105.22, "low":103.18, "latestPrice":104.43, "latestSource":"Close", "latestTime":"January 31, 2019", "latestUpdate":1548968400284, "latestVolume":55458869, "iexRealtimePrice":104.41, "iexRealtimeSize":200, "iexLastUpdated":1548968398860, "delayedPrice":104.43, "delayedPriceTime":1548968400284, "extendedPrice":104.7, "extendedChange":0.27, "extendedChangePercent":0.00259, "extendedPriceTime":1548971996269, "previousClose":106.38, "change":-1.95, "changePercent":-0.01833, "iexMarketPercent":0.03518, "iexVolume":1951043, "avgTotalVolume":40892101, "iexBidPrice":0, "iexBidSize":0, "iexAskPrice":0, "iexAskSize":0, "marketCap":801627522600, "peRatio":26.91, "week52High":116.18, "week52Low":83.83, "ytdChange":0.03368740506329105}, {"symbol":"T", "companyName":"AT&T Inc.", "primaryExchange":"New York Stock Exchange", "sector":"Communication Services", "calculationPrice":"close", "open":29.47, "openTime":1548945005315, "close":30.06, "closeTime":1548968480334, "high":30.09, "low":29.145, "latestPrice":30.06, "latestSource":"Close", "latestTime":"January 31, 2019", "latestUpdate":1548968480334, "latestVolume":50032732, "iexRealtimePrice":30.055, "iexRealtimeSize":36, "iexLastUpdated":1548968396918, "delayedPrice":30.06, "delayedPriceTime":1548968480334, "extendedPrice":30.06, "extendedChange":0, "extendedChangePercent":0, "extendedPriceTime":1548971954000, "previousClose":29.37, "change":0.69, "changePercent":0.02349, "iexMarketPercent":0.02991, "iexVolume":1496479, "avgTotalVolume":42933597, "iexBidPrice":0, "iexBidSize":0, "iexAskPrice":0, "iexAskSize":0, "marketCap":218776680000, "peRatio":8.74, "week52High":39.29, "week52Low":26.8, "ytdChange":0.03421323321311573}]
const gainersUrl = 'https://api.iextrading.com/1.0/stock/market/list/gainers'
const mostactiveUrl = 'https://api.iextrading.com/1.0/stock/market/list/mostactive'
// const losersUrl = 'https://api.iextrading.com/1.0/stock/market/list/losers'
// const inFocusUrl = 'https://api.iextrading.com/1.0/stock/market/list/inFocus'
function mockAxios() {
  jest.mock('axios')
}
function unmockAxios() {
  // jest.unmock('axios')
  jest.requireActual('axios')
}
describe('Mocked data fetch using axios', () => {
  beforeAll(() => {
    mockAxios()
  })
  afterAll(() => {
    unmockAxios()
  })
  it('fetch mock data and checks contains "symbol"', () => {
    const resp = {data: {symbol: "test1", data2: "test2"}}
    axios.get.mockResolvedValue(resp)
    return libObj.fetchData(gainersUrl)
      .then((result) => {
        expect(JSON.stringify(result))
          .toContain('symbol')
      })
  })
  it('get Sector info', () => {
    const mockSectorsData = {
      Industrials: ['General Electric Company'],
      Technology: ['Advanced Micro Devices Inc.', 'Facebook Inc.', 'Nokia Corporation Sponsored American Depositary Shares', 'Microsoft Corporation'],
      'Financial Services': ['Bank of America Corporation'],
      'Consumer Cyclical': ['Sirius XM Holdings Inc.', 'Pandora Media Inc.'],
      Energy: ['Chesapeake Energy Corporation'],
      'Communication Services': ['AT&T Inc.']
    }
    return libObj.getSector(mockStockData10)
      .then((result) => {
        expect(result).toEqual(mockSectorsData)
      })
  })
  it('get Primary Exchange info', () => {
    const mockPrimaryExchangeData = {
      'New York Stock Exchange': ['General Electric Company', 'Bank of America Corporation', 'Nokia Corporation Sponsored American Depositary Shares', 'Pandora Media Inc.', 'Chesapeake Energy Corporation', 'AT&T Inc.'],
      'Nasdaq Global Select': ['Advanced Micro Devices Inc.', 'Sirius XM Holdings Inc.', 'Facebook Inc.', 'Microsoft Corporation']
    }
    return libObj.getPrimaryExchange(mockStockData10)
      .then((result) => {
        expect(result).toEqual(mockPrimaryExchangeData)
      })
  })
})
describe('Live data fetch using axios', () => {
  beforeAll(() => {
    unmockAxios()
    jest.requireActual('axios')
  })
  afterAll(() => {
    mockAxios()
  })
  it('fetch live data and check array length', () => {
    const testMaxReturnedResults = 10
    return libObj.fetchData(mostactiveUrl)
      .then((result) => {
        // Test logic
        if (result.length > 0) {
          expect(result).toHaveLength(testMaxReturnedResults)
        } else {
          expect(result).toHaveLength(testMaxReturnedResults)
        }
      })
  })
  it('fetch live data and checks contains "companyName"', () => {
    libObj.fetchData(gainersUrl)
      .then((result) => {
        expect(JSON.stringify(result))
          .toContain('companyName')
      })
  })
  it('fetch live data and checks contains "primaryExchange"', () => {
    libObj.fetchData(gainersUrl)
      .then((result) => {
        expect(JSON.stringify(result))
          .toContain('primaryExchange')
      })
  })
  it('fetch live data and checks contains "sector"', () => {
    libObj.fetchData(gainersUrl)
      .then((result) => {
        expect(JSON.stringify(result))
          .toContain('sector')
      })
  })
})